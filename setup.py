from setuptools import setup

setup(
    name='etsyv3',
    packages=['etsyv3'],
    version='0.0.6',
    description='A package to support the Etsy Open API v3',
    install_requires=[
        "requests>=2.28.1",
        "requests-oauthlib>=1.3.1"
    ],
    author_email='zibaeiahmadreza@gmail.com',
    url='https://gitlab.com/aaronium/etsy-library',
    classifiers=[
    ],
)
